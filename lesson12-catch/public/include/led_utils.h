#ifndef __LED_UTILS_H__
#define __LED_UTILS_H__

#include "stm32f10x.h"

#define LED_PORT_RCC RCC_APB2Periph_GPIOC
#define LED_PORT GPIOC

void custom_led_init(void);
void led_on(u8 position);
void led_off(u8 position);
void led_all_off(void);
void led_lightn(u8 n);
#endif
