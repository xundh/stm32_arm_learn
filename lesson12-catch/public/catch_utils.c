#include "catch_utils.h"

#define RISING 0
#define FALLING 1


/**
 * @brief  捕获初始化
*/
void catch_gpio_init(u16 period, u16 prescaler)
{
    // GPIO 初始化
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 定时器初始化
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Period = period;
    TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);

    // 输入捕获初始化
    TIM_ICInitTypeDef TIM_ICInitStructure;
    TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
    TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
    TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
    TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM_ICInitStructure.TIM_ICFilter = 0x00;
    TIM_ICInit(TIM5, &TIM_ICInitStructure);
    TIM_OC1PolarityConfig(TIM5, TIM_ICPolarity_Rising);

    // 开启捕获和定时器中断
    TIM_ITConfig(TIM5, TIM_IT_Update | TIM_IT_CC1, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
 * 中断函数
*/
void TIM5_IRQHandler(void)
{
    // 如果捕获到，要翻转一下捕获方向，如果捕获到下降沿，就设置为上升沿，并计算捕获时间并输出
    if (TIM_GetITStatus(TIM5, TIM_IT_CC1))
    {
        if (TIM5_Capture == RISING)
        {
            printf("catch rising irq, TIM5_Capture= %d \n", TIM5_Capture);
            TIM5_Capture = FALLING;
            // 关闭定时器
            TIM_Cmd(TIM5, DISABLE);
            // 清空定时器计数
            TIM_SetCounter(TIM5, 0);
            TIM5_Overflow = 0;
            TIM_OC1PolarityConfig(TIM5, TIM_ICPolarity_Falling);
            TIM_Cmd(TIM5, ENABLE);
        }
        else
        {
            // 捕获到下降沿
            TIM5_Capture = RISING;
            // 计算 总时间
            u16 time = TIM_GetCapture1(TIM5);
            u32 total_time = TIM5_Overflow * 0xffff + time;
            printf("catch falling irq, capture time= %d, overflow count = %d \n, total=%dus \n", time, TIM5_Overflow, total_time);
            TIM_Cmd(TIM5, DISABLE);
            TIM_OC1PolarityConfig(TIM5, TIM_ICPolarity_Rising);
            TIM_Cmd(TIM5, ENABLE);
        }
        TIM_ClearITPendingBit(TIM5, TIM_IT_CC1);
    }else if(TIM_GetITStatus(TIM5, TIM_IT_Update)){
        TIM5_Overflow++;
        TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
    }
}
/**
 * @brief 使能定时器
*/
void catch_timer_enable(void)
{
    TIM_Cmd(TIM5, ENABLE);
}
