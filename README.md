# STM32 学习笔记

博客地址：
https://blog.csdn.net/xundh/category_12579197.html

目录说明：
- lesson1 基础框架
- lesson2-1_gpio 库函数操作GPIO示例1
- lesson2-2_gpio 库函数操作GPIO示例2
- lesson3_register 寄存器映射和GPIO寄存器编程
- lesson4_bit_banding 位带操作
- lesson5_clock 时钟系统
- lesson6_sys_tick SysTick 计时器
- lesson7_key 按键实验
- lesson8_uart USART通讯
- lesson9-1_exti 外部中断
- lesson9-2_timx 通用定时器
- lesson10-pwm PWM呼吸灯
- lesson11-iwgd 独立看门狗
- lesson12-catch 捕获
- lesson13-sleep-wfi 睡眠模式
- lesson13-standby 待机模式
- lesson14-adc ADC采集

本仓库从网上下载了几份STM32和arm的文档，如有侵权乃无心之举，告知即删。
邮箱： xundh@qq.com
