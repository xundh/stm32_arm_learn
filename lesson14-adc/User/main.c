#include "gpio_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "usart_utils.h"
#include "stdio.h"
#include "adc_utils.h"
#include "led_utils.h"

// 主函数
int main(void)
{
	u8 i;
	u16 value;
	GPIO_Configuration(); // 调用GPIO配置函数
	// tick 初始化
	sys_tick_init(72);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	USART3_Init(9600);
	printf("starting...");

	ADCx_Init();

	while (1) // 无限循环
	{
		i++;
		if(i%20==0){
			led_flash(0);
		}
		if(i%50==0){
			value = Get_Adc(ADC_Channel_1, 20);
			printf("value=%d\r\n", value);
			printf("votage=%f\r\n", (float)value*(3.3/4096));
		}
		delay_ms(10);
	}
}
