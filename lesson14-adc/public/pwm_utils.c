#include "pwm_utils.h"
#include "led_utils.h"

/**
 * @brief  定时器3初始化
*/
void tim3_ch1_pwm_init(u16 preriod, u16 prescaler){
    // 使能TIM3时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    // 使能LED所在端口的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    // 使能AFIO
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure; //定义GPIO初始化结构体
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //设置输出速度为50MHz
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //设置为推挽输出模式
    GPIO_Init(LED_PORT, &GPIO_InitStructure); //初始化 LED_PORT

    // 管脚重映像
    GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);
    // 定时器初始化
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Period = preriod; //设置自动重装载寄存器周期值
    TIM_TimeBaseStructure.TIM_Prescaler = prescaler; //设置时钟预分频数
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分频因子
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    // 初始化
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    // PWM模式1
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式1
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
    TIM_OC1Init(TIM3, &TIM_OCInitStructure); //根据T指定的参数初始化外设TIM3 OC1

    // 使能TIM3的CCR1寄存器预装载
    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
    // 使能TIM3的ARR寄存器预装载
    TIM_ARRPreloadConfig(TIM3, ENABLE);
    // 使能TIM3
    TIM_Cmd(TIM3, ENABLE);
}
/**
 * @brief  设置定时器3的PWM占空比
*/
void tim3_ch1_pwm_set_duty(u16 duty){
    TIM_SetCompare1(TIM3, duty);
}
