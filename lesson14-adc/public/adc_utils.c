#include "adc_utils.h"

/**
* @brief ADCx初始化端口
*/
void ADCx_Init(void){
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_ADC1, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    // 模拟输入模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    // 速度50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // 设置分频因子为6
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    // ADC1配置
    ADC_InitTypeDef ADC_InitStructure;
    // 单通道模式
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    // 扫描模式, 一次转换一个通道
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    // 连续扫描还是单次扫描
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    // 使用软件触发，不使用外部触发
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    // 结果右对齐
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    // 通道数
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    // 完成初始化
    ADC_Init(ADC1, &ADC_InitStructure);

    // 使能ADC1
    ADC_Cmd(ADC1, ENABLE);

    // 复位校准
    ADC_ResetCalibration(ADC1);
    // 等待复位校准完成
    while(ADC_GetResetCalibrationStatus(ADC1));
    // 开始校准
    ADC_StartCalibration(ADC1);
    // 等待校准完成
    while(ADC_GetCalibrationStatus(ADC1));
    // 软件触发ADC转换
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

/**
* @param ch 通道
* @param time 读取次数
* @brief 读取ADC值
*/
u16 Get_Adc(u8 ch, u8 time){
    u8 i;
    // 规则通道的规则序列进行设定，采样周期最长
    ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_239Cycles5);
    u16 value = 0;
    for(i = 0; i < time; i++){
        // 每次都用软件触发转换
        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
        // 等待转换结束
        while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));
        // 返回转换结果
        value += ADC_GetConversionValue(ADC1);
    }
    return value / time;
}
