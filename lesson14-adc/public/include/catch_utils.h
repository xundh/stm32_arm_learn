#ifndef __CATCH_UTILS_H__
#define __CATCH_UTILS_H__
#include "stm32f10x.h"
#include "stdio.h"

// 定时器溢出的次数
static u8 TIM5_Overflow = 0;
// 捕获到上升沿
static u8 TIM5_Capture = 0;
void catch_gpio_init(u16 period, u16 prescaler);
void catch_timer_enable(void);
#endif
