#include "stm32f10x.h" //包含所有的STM32F10x库的头文件


int arr[10][7] = {
	{0,1,2,3,4,5},
	{1,2},
	{0,1,6,4,3},
	{0,1,6,2,3},
	{5,6,1,2},
	{0,5,6,2,3},
	{0,5,6,4,3,2},
	{0,1,2},
	{0,1,2,3,4,5,6},
	{0,1,2,3,5,6}
};

// GPIO配置函数
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure; //定义GPIO初始化结构体

    // 开启GPIOC的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    // 设置GPIOC的模式为推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All; //选择所有的pin
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //设置输出速度为50MHz
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //设置为推挽输出模式
    GPIO_Init(GPIOC, &GPIO_InitStructure); //初始化GPIOC
}

// 延时函数
void delay(u32 i)
{
    while (i--) //当i不为0时，持续减1，实现延时
        ;
}

// 打开指定位置的LED
void on(int position)
{
	GPIO_ResetBits(GPIOC, GPIO_Pin_0 << position); //
}

// 关闭指定位置的LED
void off(int position)
{
	GPIO_SetBits(GPIOC, GPIO_Pin_0 << position);
}
void onArray(int array[], int len){
	int j;
	for(j=0;j<len;j++){
		on(array[j]);
	}
}
int count(int n){
	int len = 1;
	for (int i = 1; i < 7; i++) {
			if (arr[n][i] != '\0') {
					len++;
			} else {
					break;
			}
	}	
return len;
}
void lightn(int n){

	// 计算onArray 第二个参数，即二维数组第二维长度
	int len = count(n);
	onArray(arr[n], len);
}

void allOff(){
	GPIO_SetBits(GPIOC, GPIO_Pin_All);
}
// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数

		GPIO_SetBits(GPIOC, GPIO_Pin_All);
    while (1) //无限循环
    {
       
	   for(int i=0;i<10;i++){
		   lightn(i);
		   delay(0xfffff);
		   allOff();
		   delay(0xff);
	   }
    }
}

