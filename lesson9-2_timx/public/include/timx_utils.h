#ifndef __TIMX_UTILS_H__
#define __TIMX_UTILS_H__
#include "stm32f10x.h"
void timx_init(u16 preriod, u16 psc);
void timx_init_ms(u16 ms);
#endif
