#include "gpio_utils.h"

// GPIO配置函数
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure; //定义GPIO初始化结构体

    // 开启GPIOC的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    // 设置GPIOC的模式为推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All; //选择所有的pin
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //设置输出速度为50MHz
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //设置为推挽输出模式
    GPIO_Init(GPIOC, &GPIO_InitStructure); //初始化GPIOC
}
