#include "gpio_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "exti_utils.h"
#include "timx_utils.h"

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数
	sys_tick_init(72);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	// 定时器的时钟频率是72MHz,预分频系数是36000,
	// 所以定时器的时钟频率是72MHz/36000=2KHz,周期是1000,
	// 所以定时器的周期是1000/2KHz=0.5s
	timx_init_ms(1000);

	led_all_off();
	
    while (1) //无限循环
    {
		delay_ms(10);
    }
}
