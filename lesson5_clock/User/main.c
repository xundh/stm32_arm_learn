#include "gpio_utils.h"
#include "rcc_utils.h"
#include "stm32f10x_gpio.h"

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数
		rcc_init();
	
    while (1) //无限循环
    {
				delay(5000000);
				GPIO_ResetBits(GPIOC, GPIO_Pin_0);
				delay(5000000);
				GPIO_SetBits(GPIOC, GPIO_Pin_0);
    }
}
