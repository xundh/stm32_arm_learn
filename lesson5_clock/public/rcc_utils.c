#include "rcc_utils.h"
#include "stm32f10x_rcc.h"

void rcc_init(void){
	// 本实验通过修改 分频参数，观察系统时钟的变化
    // 1 分频， 8MHz * 9 = 72MHz
    // RCC_HSE_Config(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
    // 2 分频， 8MHz / 2 * 9 = 36MHz
	RCC_HSE_Config(RCC_PLLSource_HSE_Div2, RCC_PLLMul_9);
}
/**
  * @brief  配置外部高速时钟（HSE）和 PLL
  * @param  div: PLL输入时钟分频系数
  *         pllm: PLL倍频系数
  * @retval None
  */
void RCC_HSE_Config(uint32_t div, uint32_t pllm) {
    RCC_DeInit(); // 复位 RCC 寄存器到默认值

    // 使能外部高速时钟（HSE）
    RCC_HSEConfig(RCC_HSE_ON);

    // 等待外部时钟稳定
    if (RCC_WaitForHSEStartUp() == SUCCESS) {
        // 配置 AHB 时钟分频系数
        RCC_HCLKConfig(RCC_SYSCLK_Div1); // AHB 不分频

        // 配置 APB1 和 APB2 时钟分频系数
        RCC_PCLK1Config(RCC_HCLK_Div2); // 低速 APB1 分频为 HCLK/2 36M
        RCC_PCLK2Config(RCC_HCLK_Div1); // 高速 APB2 不分频 72M

        // 配置 PLL 输入时钟分频系数和倍频系数
        RCC_PLLConfig(div, pllm);

        // 使能 PLL
        RCC_PLLCmd(ENABLE);

        // 等待 PLL 就绪
        while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

        // 将 PLL 作为系统时钟源
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

        // 等待系统时钟源稳定
        while (RCC_GetSYSCLKSource() != 0x08);
    }
}
// 延时函数
void delay(uint32_t i)
{
    while (i--) //当i不为0时，持续减1，实现延时
        ;
}
