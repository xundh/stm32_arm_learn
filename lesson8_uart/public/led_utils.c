#include "led_utils.h"

static u8 arr[10][7] = {
	{0,1,2,3,4,5},
	{1,2},
	{0,1,6,4,3},
	{0,1,6,2,3},
	{5,6,1,2},
	{0,5,6,2,3},
	{0,5,6,4,3,2},
	{0,1,2},
	{0,1,2,3,4,5,6},
	{0,1,2,3,5,6}
};
static void turn_on_array(u8 array[], u8 len){
	u8 j;
	for(j=0;j<len;j++){
		led_on(array[j]);
	}
}
static u8 count(u8 n){
	u8 len = 1;
	for (u8 i = 1; i < 7; i++) {
			if (arr[n][i] != '\0') {
					len++;
			} else {
					break;
			}
	}	
	return len;
}


// 打开指定位置的LED
void led_on(u8 position)
{
	GPIO_ResetBits(GPIOC, GPIO_Pin_0 << position); //
}

// 关闭指定位置的LED
void led_off(u8 position)
{
	GPIO_SetBits(GPIOC, GPIO_Pin_0 << position);
}
/**
 * 显示数值 0-9
*/
void led_lightn(u8 n){
    led_all_off();
	// 计算 第二个参数，即二维数组第二维长度
	u8 len = count(n);
	turn_on_array(arr[n], len);
}
void led_all_off(void){
	GPIO_SetBits(GPIOC, GPIO_Pin_All);
}
