#include "gpio_utils.h"
#include "rcc_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "key_utils.h"
#include "usart_utils.h"
#include "stdio.h"

// 主函数
int main(void)
{
	GPIO_Configuration(); // 调用GPIO配置函数
	sys_tick_init(72);
	led_all_off();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	USART3_Init(9600);

	printf("HelloWorld!\n");
	while (1) // 无限循环
	{
		delay_ms(20);
	}
}
