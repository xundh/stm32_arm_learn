#ifndef __PWM_UTILS_H__
#define __PWM_UTILS_H__

#include "stm32f10x.h"

void tim3_ch1_pwm_init(u16 preriod, u16 prescaler);
void tim3_ch1_pwm_set_duty(u16 duty);
#endif
