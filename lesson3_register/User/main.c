#include "stm32f10x.h"

void SystemInit(){
}

// GPIO配置函数
void GPIO_Configuration(void)
{
    // 开启GPIOC的时钟
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

    // 设置GPIOC的模式为推挽输出
    GPIOC->CRL = 0x33333333; // 配置低八位引脚为推挽输出模式
    GPIOC->CRH = 0x33333333; // 配置高八位引脚为推挽输出模式
}

// 延时函数
void delay(uint32_t i)
{
    while (i--) //当i不为0时，持续减1，实现延时
        ;
}

// 打开指定位置的LED
void on(int position)
{
    GPIOC->BSRR = (1 << position); // 设置对应的位，输出低电平
}

// 关闭指定位置的LED
void off(int position)
{
    GPIOC->BRR = (1 << position); // 清除对应的位，输出高电平
}

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数

    int j;

    while (1) //无限循环
    {
        for (j = 0; j < 8; j++) //遍历0到7号位
        {
            on(j); //打开j号位的LED
            delay(0xfffff); //延时
            off(j); //关闭j号位的LED
            delay(0xfffff); //延时
        }
    }
}
