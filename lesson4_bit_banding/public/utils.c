#include "utils.h"

// GPIO配置函数
void GPIO_Configuration(void)
{
    // 开启GPIOC的时钟
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

    // 设置GPIOC的模式为推挽输出
    GPIOC->CRL = 0x33333333; // 配置低八位引脚为推挽输出模式
    GPIOC->CRH = 0x33333333; // 配置高八位引脚为推挽输出模式
}
// 延时函数
void delay(uint32_t i)
{
    while (i--) //当i不为0时，持续减1，实现延时
        ;
}
void allOff(void){
	   for (int j = 0; j < 8; j++) 
			{
					PCout(j) = 1;
			}
}
