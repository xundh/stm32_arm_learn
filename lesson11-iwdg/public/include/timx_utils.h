#ifndef __TIMX_UTILS_H__
#define __TIMX_UTILS_H__
#include "stm32f10x.h"
void tim4_init(u16 preriod, u16 psc);
void tim4_init_ms(u16 ms);
#endif
