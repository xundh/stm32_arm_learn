#include "standby_utils.h"

/**
* @brief  进入待机模式
*/
void Standby_Enter(void){
	// 开启时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	// 设置唤醒源,使用WakeUpPin唤醒
	PWR_WakeUpPinCmd(ENABLE);
	// 清空标志
	PWR_ClearFlag(PWR_FLAG_WU);
	// 进入待机模式
	PWR_EnterSTANDBYMode();
}
