#include "stdint.h"
#include "sys_tick_utils.h"
#include "system_stm32f10x.h"

// 每 us 跳动次数
u8 tick_us;
// 每 ms 跳动次数
u16 tick_ms;

/**
 * SysTick初始化
 * @param SYSCLK 频率，一般值72
*/
void sys_tick_init(u8 SYSCLK){
	// 设置时钟源, 使用系统时钟的八分频
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
    // 72 / 8 = 9 1us跳到次数
    tick_us = SYSCLK / 8;
    // 1ms 跳到次数
    tick_ms = (u16)tick_us * 1000;
}
/**
 * 微秒延时
*/
void delay_us(u32 us){
    u32 temp;
    // 重装载初始值
    SysTick->LOAD = us * tick_us;
    // 清除当前寄存器的值
    SysTick->VAL= 0x00;
    // 打开计时器,最低位开启
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
    // 倒计时,通过CTRL第16位来判断
    do{
        temp = SysTick->CTRL;
    }while((temp&0x01) && !(temp&(1<<16)));
    // 判断 CTRL 第16位
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    // 清空计时器
    SysTick->VAL = 0x00; 
}
/**
 * 毫秒延时
*/
void delay_ms(u32 ms)
{
    delay_us(ms*1000);
}
/**
 * 秒延时
*/
void delay_second(u32 second){
    for(int i=0;i<second;i++){
        delay_ms(1000);
    }
}
