#ifndef __rcc_utils_H__
#define __rcc_utils_H__
#include "stdint.h"
void rcc_init(void);
/**
  * @brief  配置外部高速时钟（HSE）和 PLL
  * @param  div: PLL输入时钟分频系数
  *         pllm: PLL倍频系数
  * @retval None
  */
void RCC_HSE_Config(uint32_t div, uint32_t pllm);
void delay(uint32_t i);
#endif
