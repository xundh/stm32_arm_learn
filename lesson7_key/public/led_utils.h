#ifndef __led_utils_H__
#define __led_utils_H__

#include "stm32f10x.h"

void led_on(u8 position);
void led_off(u8 position);
void led_all_off(void);
void led_lightn(u8 n);
#endif
